#include <cstdio>
#include <iostream>
#include <vector>
#include <map>


#include "vtkSmartPointer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkObjectFactory.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkAxes.h"
#include "vtkAxesActor.h"
#include "vtkImageData.h"
#include <vtkExtractEdges.h>
#include "vtkVector.h"
#include "vtkTriangle.h"
#include "vtkTriangleFilter.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkQuadricClustering.h"
#include "vtkPLYReader.h"
#include "vtkPolyDataWriter.h"

#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkMatrix4x4.h"
#include "vtkMatrix3x3.h"

#define BOOST_UBLAS_NDEBUG 1
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "mymath.h"
#include "cp_time.h"

using namespace std;
//using namespace boost::numeric;

#define vsp_new(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

// approximation method
#define AMD 0
#define LINDSTROM 1
//#define APPROX LINDSTROM
#define APPROX AMD

vtkRenderer *ren;
vtkRenderWindow *renWin;

/// for mesh simplification
vtkSmartPointer<vtkPolyData> data;
vtkSmartPointer<vtkPolyData> output_data;
double origin[3];
int xdim, ydim, zdim;
void set_origin_and_dim();
float grid_width = .5;
float one_over_grid_width = 2.;

bool bShowGrids = false;
#ifdef PROFILING
bool bShowOriginal = false;
#else
bool bShowOriginal = true;
#endif
bool bShowOutput = false;
bool bUseVTKSimplify = false;

typedef vtkVector<float, 4> vtkVector4f;
typedef ublas::vector<float> vectorf;
typedef ublas::vector<double> vectord;
#if 1

typedef ublas::bounded_matrix<float, 4, 4> matrixf;
typedef ublas::bounded_matrix<double, 4, 4> matrixd;
#else
typedef ublas::matrix<float, ublas::column_major> matrixf;
typedef ublas::matrix<double, ublas::column_major> matrixd;
#endif
inline void make_plane(vtkPoints *pts, vtkIdType* pointIds, double plane[4])
{
    double p0[3], p1[3], p2[3];
    pts->GetPoint(pointIds[0], p0);
    pts->GetPoint(pointIds[1], p1);
    pts->GetPoint(pointIds[2], p2);
    vtkVector3d v1 (p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]);
    vtkVector3d v2 (p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]);
    vtkVector3d normal = v1.Cross(v2);    // surface normal
    #if 1// This is what DeCore & Tatarchuk 2007 does
        normal.Normalize();
    #else
        // This is what Lindstrom 2000 does
        /// let the weight proportional to the triangle size
        /// since we will compute its qudric form, now we multiply the sqrt of the triangle size
        double w = normal.Normalize();
        w = sqrt(w);
        normal = vtkVector3d(normal[0] * w, normal[1]*w, normal[2]*w);
    #endif
    double d = -normal.Dot(*(vtkVector3d*)p0);

    plane[0] = normal[0];
    plane[1] = normal[1];
    plane[2] = normal[2];
    plane[3] = d;

    // should be close to zero: (checked!)
    //cout << normal.Dot(*(vtkVector3d*)p1)+d << "\t" << normal.Dot(*(vtkVector3d*)p2)+d << endl;
}


int get_cluster_id( double *p )
{
    /// determine grid resolution for clustering
    int x = (p[0] - origin[0]) * one_over_grid_width;
    int y = (p[1] - origin[1]) * one_over_grid_width;
    int z = (p[2] - origin[2]) * one_over_grid_width;
    x = min(x, xdim-1);
    y = min(y, ydim-1);
    z = min(z, zdim-1);
    return x+xdim*(y+ydim*z);
}

inline void rotate(int &i1, int &i2, int &i3)
{
    int temp=i1; i1 = i2; i2 = i3; i3 = temp;
}

// assume i1 != i2 != i3
void sort_ids(int &i1, int &i2, int &i3)
{
    while (i1>i2 || i1>i3)
        rotate(i1, i2, i3);
}
size_t ihash(int i1, int i2, int i3, int size)
{
    assert(i1 < size && i2 < size && i3<size);
    return i1+size*(i2+(size_t)size*i3);
}
#if 0
void unhash(size_t id, int size, int &i1, int &i2, int &i3)
{
    i1 = id%size;
    id /= size;
    i2 = id%size;
    i3 = id/size;
}
#endif

/// determine grid resolution for clustering
void set_origin_and_dim()
{
    one_over_grid_width = 1. / grid_width;

    double *bounds = data->GetBounds();
    xdim = ceil((bounds[1]-bounds[0])*one_over_grid_width);
    ydim = ceil((bounds[3]-bounds[2])*one_over_grid_width);
    zdim = ceil((bounds[5]-bounds[4])*one_over_grid_width);
    if (0) {
        origin[0] = bounds[0];
        origin[1] = bounds[2];
        origin[2] = bounds[4];
    }else {
        origin[0] = (bounds[1]+bounds[0])*0.5 - grid_width*(xdim)*.5;
        origin[1] = (bounds[3]+bounds[2])*0.5 - grid_width*(ydim)*.5;
        origin[2] = (bounds[5]+bounds[4])*0.5 - grid_width*(zdim)*.5;
    }
}

void make_square(const double vec[4], double quad[16])
{
    quad[0] = vec[0]*vec[0];
    quad[1] = vec[0]*vec[1];
    quad[2] = vec[0]*vec[2];
    quad[3] = vec[0]*vec[3];
    quad[4] = vec[1]*vec[0];
    quad[5] = vec[1]*vec[1];
    quad[6] = vec[1]*vec[2];
    quad[7] = vec[1]*vec[3];
    quad[8] = vec[2]*vec[0];
    quad[9] = vec[2]*vec[1];
    quad[10] = vec[2]*vec[2];
    quad[11] = vec[2]*vec[3];
    quad[12] = vec[3]*vec[0];
    quad[13] = vec[3]*vec[1];
    quad[14] = vec[3]*vec[2];
    quad[15] = vec[3]*vec[3];
}

/// pull the solution back into the cell if falling outside of cell
inline void pull_back(vectorf &p, int x, int y, int z)
{
    vectorf center(3);
    center[0] = origin[0]+grid_width*(x+.5);
    center[1] = origin[1]+grid_width*(y+.5);
    center[2] = origin[2]+grid_width*(z+.5);
#if 1
    {
        int dim;
        for (dim=0; dim<3; dim++)
        {
            p[dim] = min((float)center[dim]+0.5f*grid_width, p[dim]);
            p[dim] = max((float)center[dim]-0.5f*grid_width, p[dim]);
        }
    }
#else
    {
    //while(1) {
        float dist = ublas::norm_2(center-p);
        if (dist > grid_width/2.f) {
            p = center + (p - center) / dist * (grid_width*.5f*.95f);
        }// else
           // break;
    //}
    }
#endif
}

/// the main algorithm
void simplify(vtkSmartPointer<vtkPolyData> data)
{
    /// determine grid resolution for clustering call this before the timer
    /// as this is already cached in the vtk version.
    set_origin_and_dim();


    Timer timer1, timer2, timer3;
    timer1.start();

    int i, x,y,z;

    /// pass 1 : Cluster-quadric map generation
    /// For each triangle, compute error quadric and add to each cluster
    /// Each cluster stores:
    ///   sum of error quadric
    ///   avearge vertex position
    ///   vertex count
    struct ClusterInfo{
        int vcount;
        vectorf p; // average point
        ClusterInfo(): vcount(0), p(ublas::zero_vector<float>(3)) {}
    };

#ifndef PROFILING
    cout << "Pass 1: " << data->GetNumberOfCells() << " cells" << endl;
#endif

    int clusters = xdim*ydim*zdim;
    vector<ClusterInfo> clustermap(clusters);
    vector<matrixd> clusterQs(clusters);

    vtkPoints* points = data->GetPoints();
    vtkCellArray* tris = data->GetPolys();

    vtkIdType npts;
    vtkIdType* pointIds;

    tris->InitTraversal();
    int cellCount = 0;
    double quadric4x4[16];
    while( tris->GetNextCell(npts, pointIds) )
    {
        if(npts != 3) { continue; } //not a triangle so skip

        double* tri_points[3] = {
                    points->GetPoint(pointIds[0]),
                    points->GetPoint(pointIds[1]),
                    points->GetPoint(pointIds[2])
                    };

        vtkTriangle::ComputeQuadric(tri_points[0],
                                    tri_points[1],
                                    tri_points[2],
                                    (double(*)[4])(quadric4x4) );

        int cids[3] =
            { get_cluster_id(tri_points[0]),
              get_cluster_id(tri_points[1]),
              get_cluster_id(tri_points[2])
            };

        for (int j=0; j<3; j++)
        {
            matrixd& q = clusterQs[cids[j]];
            for (int k=0; k<16; k++)
                q.data()[k] += quadric4x4[k];

            ClusterInfo &cluster = clustermap[cids[j]];
            cluster.p[0] += tri_points[j][0];
            cluster.p[1] += tri_points[j][1];
            cluster.p[2] += tri_points[j][2];
            cluster.vcount++;
        }

        ++cellCount;
    }

    /// pass 2 : Optimal representative computation
    /// For each cluster, compute the representative vertex using Garland and Heckbert
    ///
    timer1.end();

    cout << "Time (ms): " << timer1.getElapsedMS() << endl;
    cout << "pass 2" << endl;

    timer2.start();

    int num_points = 0;
    int empty_count = 0, regular_count=0, degenerate_count = 0;  // some statistics
    int cid = -1;
    for (z=0; z<zdim; z++)
        for (y=0; y<ydim; y++)
            for (x=0; x<xdim; x++)
    {
        cid ++;                                 //int cid = x+xdim*(y+ydim*z);
        ClusterInfo &cluster = clustermap[cid];
        matrixd& cluseterq = clusterQs[cid];

        if (cluster.vcount==0) {
            empty_count++;
            continue;
        }

#if APPROX==AMD // This is AMD algorithm
        /// solve Qx = [0 0 0 1]T
        matrixd inv(4,4);
        bool b;
        b = fabs(vtkMatrix4x4::Determinant(&cluseterq.data()[0])) > 1e-6;

        if (b)
            vtkMatrix4x4::Invert(&cluseterq(0,0) , &inv(0,0));

        //    b = InvertMatrix(cluseterq, inv);

        //cout << inv << endl;

        if (!b || inv(3,3)!=inv(3,3) /* check nan */) {
#ifndef PROFILING
            printf("Use avg\n");
#endif
            cluster.p /= (float)cluster.vcount;
            degenerate_count ++;
        } else {
            cluster.p[0] = inv(0,3)/inv(3,3);
            cluster.p[1] = inv(1,3)/inv(3,3);
            cluster.p[2] = inv(2,3)/inv(3,3);

            pull_back(cluster.p, x, y, z);

            regular_count ++;
        }
#elif APPROX==LINDSTROM
        /// solve Ax = b  where Q = [A b; bT c] to minimize
        /// Note A is symmetric

        cluster.p /= (float)cluster.vcount;  // get mean position

        /// Diagonalize to get A = Q*D*QT
        //double A[3][3], Q[3][3], D[3][3];
        matrixd A(3,3), Q(3,3), D(3,3);
        {
            int i,j;
            for (i=0; i<3; i++)
                for (j=0; j<3; j++)
                    A(i,j) = cluseterq(i,j);

        }
        Diagonalize( (double (*)[3])&A.data()[0], (double (*)[3])&Q.data()[0], (double (*)[3])&D.data()[0] );
        //cout << A << endl << Q << endl << D << endl << "---" << endl;

        double dmax = max(D(0,0), max(D(1,1), D(2,2)));
        if (dmax == 0 || dmax != dmax) { // check 0 or nan
            degenerate_count ++;
        } else {

            if (!(D(0,0)>=0 && D(1,1)>=0 && D(2,2)>=0))
                cout << D << endl;
            matrixd invD = ublas::zero_matrix<double>(3,3);
            invD(0,0) = D(0,0)>1e-3*dmax ? 1./D(0,0) : 0;
            invD(1,1) = D(1,1)>1e-3*dmax ? 1./D(1,1) : 0;
            invD(2,2) = D(2,2)>1e-3*dmax ? 1./D(2,2) : 0;

            vectorf b(3);
            b[0] = -cluseterq(0,3); b[1] = -cluseterq(1,3); b[2] = -cluseterq(2,3);


            D = ublas::prod(Q, invD);
            D = ublas::prod(D, ublas::trans(Q));
            cluster.p = cluster.p + ublas::prod(D , (b - ublas::prod(A, cluster.p)));

            //cout << cluster.p << endl;

            pull_back(cluster.p, x,y,z);

        }
#endif

        /// check get_cluster_id
#ifndef PROFILING
        {
            double p[3];
            p[0] = clustermap[cid].p[0];
            p[1] = clustermap[cid].p[1];
            p[2] = clustermap[cid].p[2];
            int id = get_cluster_id(p);
            if (id != cid) {
                vectorf center(3);
                center[0] = origin[0]+grid_width*(x+.5);
                center[1] = origin[1]+grid_width*(y+.5);
                center[2] = origin[2]+grid_width*(z+.5);

                printf("cluster id changed: %d -> %d ", cid, id);
                cout << center  ;
                printf(" -> %lf %lf %lf\n", p[0], p[1], p[2]);
                cout << inv << endl;
                cout << clustermap[cid].p << endl;
            }
        }
#endif

        num_points++;
    }

    vsp_new(vtkPoints , out_pts);
    out_pts->SetNumberOfPoints(clusters);
    for (i=0; i<clusters; i++)
    {
        //out_pts->InsertNextPoint(clustermap[i].p[0], clustermap[i].p[1], clustermap[i].p[2]);
        out_pts->SetPoint(i, clustermap[i].p[0], clustermap[i].p[1], clustermap[i].p[2]);
    }

    timer2.end();
    cout << "Time (ms): " << timer2.getElapsedMS() << endl;

    /// pass 3 : Decimated mesh generation
    /// For each original triangle, only output vertices from three different clusters
    cout << "pass 3" << endl;
    timer3.start();

    vsp_new(vtkCellArray, out_cells);  /// the output cell array

    map<size_t, int> trimap;

    tris->InitTraversal();
    while( tris->GetNextCell(npts, pointIds) )
    {
        if(npts != 3) { continue; } //not a triangle so skip

        double p[3];
        int cid0, cid1, cid2;

        /// get cluster id for each vertex
        points->GetPoint(pointIds[0], p);
        cid0 = get_cluster_id(p);
        //printf("%lf %lf %lf ->", p[0], p[1], p[2]);
        //cout << clustermap[cid0].p << endl;

        points->GetPoint(pointIds[1], p);
        cid1 = get_cluster_id(p);

        points->GetPoint(pointIds[2], p);
        cid2 = get_cluster_id(p);

        //printf("%d %d %d\n", cid0, cid1, cid2);

        if (cid0 == cid1 || cid0 == cid2 || cid1 == cid2 )
            continue;

        /// make the first id smallest
        /// note that all ids are now unique
        sort_ids(cid0, cid1, cid2);

#if 0
        if (0) { // check hash function
            size_t id = ihash(cid0, cid1, cid2, clusters);
            int i1, i2,i3;
            unhash(id, clusters, i1, i2, i3);
            // printf ("%d %d , %d %d , %d %d\n", cid0, i1, cid1, i2, cid2, i3);
        }
#endif

        size_t hashed = ihash(cid0, cid1, cid2, clusters);

        map<size_t, int>::iterator it;
        it = trimap.find(hashed);
        if ( it == trimap.end() )
        {
            trimap.insert(pair<size_t, int>(hashed, 1));

            vsp_new(vtkTriangle, new_tri);
            new_tri->GetPointIds()->SetId(0, cid0);
            new_tri->GetPointIds()->SetId(1, cid1);
            new_tri->GetPointIds()->SetId(2, cid2);

            out_cells->InsertNextCell(new_tri);
        }
        // else do nothing

    }

#if 0
    vsp_new(vtkCellArray, out_cells);
    for (map<size_t, int>::iterator it = trimap.begin(); it != trimap.end() ; ++it)
    {
        size_t hashed = it->first;
        int cid0, cid1, cid2;
        unhash(hashed, clusters, cid0, cid1, cid2);

        vsp_new(vtkTriangle, new_tri);
        new_tri->GetPointIds()->SetId(0, cid0);
        new_tri->GetPointIds()->SetId(1, cid1);
        new_tri->GetPointIds()->SetId(2, cid2);

        out_cells->InsertNextCell(new_tri);
    }
#endif

    output_data = vtkPolyData::New();
    output_data->SetPoints(out_pts);
    output_data->SetPolys(out_cells);

    timer3.end();

    cout << "Time (ms): " << timer3.getElapsedMS() << endl;

    cout << "Total time (ms): " << (timer1.getElapsedMS()+timer2.getElapsedMS()+timer3.getElapsedMS()) << endl;
    cout << "Number of output points: " << num_points << " " << output_data->GetNumberOfPoints() << endl;
    cout << "Number of output triangles: " << trimap.size() << " " << output_data->GetNumberOfCells() << endl;
    cout << "* Regularly minimized cells: " << regular_count << ", degenerate cells: " << degenerate_count << ", empty cells: " << empty_count << endl;
}

/// the vtk version
void vtk_simplify(vtkSmartPointer<vtkPolyData> data)
{
    Timer timer;
    timer.start();

    set_origin_and_dim();   // compute xdim, ydim, zdim

    vsp_new(vtkQuadricClustering, filter);
    filter->SetInputData(data);
    filter->SetNumberOfXDivisions(xdim);
    filter->SetNumberOfYDivisions(ydim);
    filter->SetNumberOfZDivisions(zdim);
    filter->AutoAdjustNumberOfDivisionsOff();
    filter->Update();

    output_data = vtkPolyData::New();
    output_data->DeepCopy(filter->GetOutput());

    timer.end();
    cout << "Time (ms): " << timer.getElapsedMS() << endl;

    cout << "Number of output points: " << output_data->GetNumberOfPoints() << endl;
    cout << "Number of output cells: " << output_data->GetNumberOfCells() << endl;

    filter->GetDivisionOrigin(origin);
    double *spacing = filter->GetDivisionSpacing();
    printf ("Spacing: %lf %lf %lf\n", spacing[0], spacing[1], spacing[2]);

}

void draw()
{
    ren->RemoveAllViewProps();

    // draw data
    if (bShowOriginal)
    {
        vsp_new(vtkPolyDataMapper, polymapper);
        polymapper->SetInputData(data);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(polymapper);

        ren->AddActor(polyactor);
    }

    // draw data
    if (bShowOutput)
    {
        vsp_new(vtkPolyDataMapper, polymapper);
        polymapper->SetInputData(output_data);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(polymapper);

        ren->AddActor(polyactor);
    }


    // axes
    vsp_new(vtkAxesActor, axes);
    ren->AddActor(axes);

    if (bShowGrids)
    {
        cout << "grid width=" << grid_width << endl;
        set_origin_and_dim();

        vsp_new(vtkImageData, image);
        image->SetDimensions(xdim+1, ydim+1, zdim+1); // grids are larger than cells by one in each dimension
        image->SetOrigin(origin[0], origin[1], origin[2]);
        image->SetSpacing(grid_width, grid_width, grid_width);

        vsp_new(vtkExtractEdges, edges);
        edges->SetInputData(image);

        vsp_new(vtkPolyDataMapper, polymapper);
        polymapper->SetInputConnection(edges->GetOutputPort());

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(polymapper);

        ren->AddActor(polyactor);
    }

    ren->SetBackground(0,0,.5); // Background color
    renWin->Render();
}

// Define interaction style
class KeyPressInteractorStyle : public vtkInteractorStyleTrackballCamera
{
  public:
    static KeyPressInteractorStyle* New();
    vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleTrackballCamera);

    virtual void OnKeyPress()
    {
      // Get the keypress
      vtkRenderWindowInteractor *rwi = this->Interactor;
      std::string key = rwi->GetKeySym();

      // Output the key that was pressed
      std::cout << "Pressed " << key << std::endl;

      // Handle an arrow key
      if(key == "Up")
        {
        std::cout << "The up arrow was pressed." << std::endl;
        }

      // Handle a "normal" key
      if(key == "s")
        {
          if (bUseVTKSimplify)
              vtk_simplify(data);
          else
            simplify(data);
          bShowOriginal = false;
          bShowOutput = true;
          draw();
        }
      if (key == "o" )
      {
          bShowOriginal = !bShowOriginal ;
          bShowOutput = !bShowOutput;
          draw();
      }
      if (key=="minus") {
          grid_width *= .5;
          cout << "grid width=" << grid_width << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="plus") {
          grid_width *= 2.;
          cout << "grid width=" << grid_width << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="g") {
          bShowGrids = ! bShowGrids;
          draw();
      }
      if (key=="v") {
          bUseVTKSimplify = ! bUseVTKSimplify;
          cout << "Use vtk = " << bUseVTKSimplify << endl;
      }
      if (key=="z") {
          cout << "Saving to output.vtk ..." << endl;
          vsp_new(vtkPolyDataWriter , writer);
          writer->SetFileName("output.vtk");
          writer->SetInputData(output_data);
          writer->Write();
      }

      // Forward events
      vtkInteractorStyleTrackballCamera::OnKeyPress();
    }

};
vtkStandardNewMacro(KeyPressInteractorStyle);

void load_input(const char *filename)
{
    printf("Loading file: %s\n", filename);

    int len = strlen(filename);
    const char *ext = filename + (len-3);

    vsp_new(vtkPolyData, data_in);
    if (strcasecmp(ext, "ply")==0)
    {
        vsp_new(vtkPLYReader, reader);
        reader->SetFileName(filename);
        reader->Update();
        data_in->DeepCopy(reader->GetOutput());
    } else {
        vsp_new(vtkXMLPolyDataReader,reader);
        reader->SetFileName(filename);
        reader->Update();
        data_in->DeepCopy(reader->GetOutput());
    }

    // triangulate
    vsp_new(vtkTriangleFilter, tri);
    tri->SetInputData(data_in);
    tri->Update();

    data = vtkSmartPointer<vtkPolyData>::New();
    data->DeepCopy(tri->GetOutput());
}


int main( int argc, char **argv )
{
    if (argc>1)
        load_input(argv[1]);
    else
        load_input(DATA_FILE);

    printf("Keys:\n"
           "g: Toggle showing grids\n"
           "+/-: Increase/decrease grid size by 2\n"
           "<<< s: Simplify mesh >>>\n"
           "o: Toggle showing orginal model or simplified model\n"
           "v: Toggle using VTK simplification filter\n"
           "z: Save output data\n"
           );

    // Visualize
    vsp_new(vtkRenderer, ren);
    vsp_new(vtkRenderWindow, renWin);
    ::ren = ren.GetPointer();
    ::renWin = renWin.GetPointer();

    renWin->AddRenderer(ren);
    renWin->SetSize(800,600);

    vsp_new(vtkRenderWindowInteractor, renderWindowInteractor );
    renderWindowInteractor->SetRenderWindow(renWin);

    vsp_new(KeyPressInteractorStyle, style);
    style->SetCurrentRenderer(ren);
    renderWindowInteractor->SetInteractorStyle(style);

    draw();

    ren->ResetCamera();

    renWin->Render();

    renderWindowInteractor->Start();

    return EXIT_SUCCESS;

}
